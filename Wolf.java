public class Wolf{
	
	private Double speed;
	private String name;
	private int weight;
	
	public void goEat(){
		if (this.weight <= 60){
			System.out.println("Feed " + this.name + "!");
		} else {
			System.out.println("Don't overfeed " + this.name + "!!");
		}
	}
	
	public void goChase(){
		if(this.speed >= 5){
			System.out.println(this.name + "! I choose you!");
		} else {
			System.out.println(this.name + ", stay home!");
		}
	}
	
	public Wolf(String name, double speed, int weight){
		this.name = name;
		this.speed = speed;
		this.weight = weight;
	}
	
	//The three setter methods
	public void setName(String newName){
		this.name = newName;
	}
	
	public void setSpeed(double newSpeed){
		this.speed = newSpeed;
	}
	
	public void setWeight(int newWeight){
		this.weight = newWeight;
	}
	
	//The three getter methods
	public String getName(){
		return this.name;
	}
	
	public double getSpeed(){
		return this.speed;
	}
	
	public int getWeight(){
		return this.weight;
	}
}



