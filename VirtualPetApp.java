import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){
		
		Scanner reader = new Scanner(System.in);
		Wolf[] pack = new Wolf[1];
		
		for(int i = 0; i < pack.length; i++){
			
			System.out.println("Give your wolf a name");
			String name = reader.nextLine();

			System.out.println("How fast is your wolf?");
			double speed = Double.parseDouble(reader.nextLine());
			
			
			System.out.println("How much does your wolf weights?");
			int weight = Integer.parseInt(reader.nextLine());
			
			pack[i] = new Wolf(name, speed, weight);
			
			System.out.println("Your pet's name is " + pack[i].getName());
			System.out.println(pack[i].getName() + " has a speed of " + pack[i].getSpeed() + " km/h");
			System.out.println(pack[i].getName() + " has a weight of " + pack[i].getWeight() + " kg");
			
		}
		
		System.out.println("Update the name of the last animal: ");
		pack[pack.length -1].setName(reader.nextLine());

		System.out.println("Your pet's name is " + pack[pack.length -1].getName());
		System.out.println(pack[pack.length -1].getName() + " has a speed of " + pack[pack.length -1].getSpeed() + " km/h");
		System.out.println(pack[pack.length -1].getName() + " has a weight of " + pack[pack.length -1].getWeight() + " kg");
	}
	
}